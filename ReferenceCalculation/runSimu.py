from IRCpkg.NewtonMethod.algorithm import evaluate_mse_loss
import subprocess
import os
import pandas as pd

folder = "run"

FF_df = pd.read_csv("exampleFF_DF.csv", index_col=0)
desired_pairs = FF_df["Eps_AB"].copy()
desired_pairs[:] = 0.
para_file = "defaultParaFile.csv"
protein_file = "exampleProteinDF.csv"

loss, dL_deps, dL_deps_err, mean_pair_numbers = evaluate_mse_loss(FF_df, desired_pairs, para_file, protein_file, run_folder=folder, seed=42, verbose=True, delete_evidence=False)

print("Gradient")
print(dL_deps)
print()
print("Gradient Err estimate")
print(dL_deps_err)
print()