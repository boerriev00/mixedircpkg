import matplotlib.pyplot as plt

from IRCpkg.getBasePath import getBasePath
import numpy as np
import pandas as pd
import os
import subprocess
from multipletau import autocorrelate
from pymbar.timeseries import integrated_autocorrelation_time
import pymbar

irc_path = getBasePath()


def simple_exponential(t, tau, A):
    return A*np.exp(-t/tau)


def estimate_tau_autocorrelate(timeseries, dt, n_points=8, folder="./", title="Example", output=False, cutoff_t=400.):
    tmp_autocorr_arr = autocorrelate(timeseries, m=n_points, normalize=True)
    lag_times = tmp_autocorr_arr[:,0]
    autocorr_vals = tmp_autocorr_arr[:,1]
    try:
        tau = integrated_autocorrelation_time(timeseries)
    except pymbar.utils.ParameterError:
        tau = 1

    lag_times *= dt
    tau *= dt

    fit_func_vals = simple_exponential(lag_times, tau, lag_times[0])
    
    if output:
        plt.figure(figsize=[6, 3])
        plot_title = str(title) + " tau: " + str(round(tau,3)) + " ns" 
        plt.title(plot_title)
        plt.plot(lag_times, fit_func_vals, "r-", alpha=0.75, label="fit")
        plt.plot(lag_times, autocorr_vals, "bx-", alpha=0.75, label="measured")
        plt.xlabel("T [ns]")
        plt.ylabel("<X(t)X(t+T)> (normalized)")
        plt.legend()
        plt.xlim([0, 40*tau])
        plt.grid()

        plt.savefig(f"{folder}/{title}.png", bbox_inches='tight')
        plt.close()

    return tau


def obtain_pairs_jacobian(FF_df, para_file, protein_file, run_folder, seed=42, verbose=False, example_derivative="UL83-UL82", delete_evidence=False):


    if not os.path.exists(run_folder):
        os.mkdir(run_folder)

    FF_df_file = f"./{run_folder}/FF_df.csv"
    FF_df.to_csv(FF_df_file)

    para_df = pd.read_csv(para_file, index_col=0).iloc[:,0]
    production_T = float(para_df["production time [mu s]"]) * 1000
    sampling_dt = float(para_df["sampling timestep [ns]"])

    run_string = f"python -u {irc_path}/run_mixed_simulation.py --parameterFile {para_file} --proteinFile {protein_file}" \
                 f" --forceFieldFile {FF_df_file} --folder {run_folder} --seed {seed} --discardTrajectory --calcDerivative > ./{run_folder}/jacobianRun.txt"
    subprocess.run(run_string, shell=True)

    file_list = os.listdir(run_folder)
    derivative_file = [ele for ele in file_list if ele[-10:] == "EPS_AB.txt"][0]
    IRC_file = [ele for ele in file_list if ele[-10:] == "IRC_AB.txt"][0]

    derivative_df = pd.read_csv(f"./{run_folder}/{derivative_file}")
    pair_df = pd.read_csv(f"./{run_folder}/{IRC_file}")

    derivative_df = derivative_df.astype(float)
    pair_df = pair_df.astype(float)

    if delete_evidence:
        os.remove(f"./{run_folder}/{derivative_file}")
        os.remove(f"./{run_folder}/{IRC_file}")
        std_file = [ele for ele in file_list if ele[-10:] == "stdout.txt"][0]
        os.remove(f"./{run_folder}/{std_file}")

    pair_columns = []
    for i, row in FF_df.iterrows():
        pair_columns.append(row["ProteinA"] + "-" + row["ProteinB"])

    derivative_df = derivative_df.loc[:, pair_columns]
    pair_df = pair_df.loc[:, pair_columns]

    len_irc_df = pair_df.shape[0]
    len_der_df = derivative_df.shape[0]

    # for some reason, sometimes openMM doesnt save everything right away resulting in slightly different lenght
    # trajectories?
    if len_irc_df <= len_der_df:
        len_traj = len_irc_df
    else:
        len_traj = len_der_df

    mean_pair_numbers = pair_df.mean()
    mean_derivative_numbers = derivative_df.mean()
    
    if verbose:
        print(f"mean pair numbers (first 10%)  {pair_df.loc[:int(0.1*len_traj)].mean()} \t total  {pair_df.mean()}")
        if not os.path.exists(f"./{run_folder}/DerivativePics"):
            os.mkdir(f"./{run_folder}/DerivativePics")
        if not os.path.exists(f"./{run_folder}/PairPics"):
            os.mkdir(f"./{run_folder}/PairPics")
        if not os.path.exists(f"./{run_folder}/CorrPics"):
            os.mkdir(f"./{run_folder}/CorrPics")
            
        smoothing_len = int(len_traj/1000)
        derivative_smoothing_filter = (1/(smoothing_len*25))*np.ones(smoothing_len*25)
        pair_smoothing_filter = (1/smoothing_len)*np.ones(smoothing_len)
    
    jacobian_array = pd.DataFrame(index=mean_derivative_numbers.index, columns=list(mean_pair_numbers.index))
    jacobian_error_array = pd.DataFrame(index=mean_derivative_numbers.index, columns=list(mean_pair_numbers.index))

    for row in jacobian_array.index:
        for col in jacobian_array.columns:

            zero_centered_pair_nums = pair_df.loc[:len_traj, row] - mean_pair_numbers[row]
            zero_centered_derivative = derivative_df.loc[:len_traj, col] - mean_derivative_numbers[col]

            cov_array = zero_centered_pair_nums.values * zero_centered_derivative.values

            jacobian_array.loc[row, col] = -cov_array.mean()

            plot=False
            if verbose and (row==col):
                plot=True
            tau_estimate = estimate_tau_autocorrelate(-cov_array, sampling_dt, folder=f"./{run_folder}/CorrPics", title=f"{row}-{col}-Corr", output=plot)

            if tau_estimate < 0:
                tau_estimate = 0
            if tau_estimate > 1e5:
                tau_estimate = 1e5
            
            effective_samples = production_T/(1+2*tau_estimate)
            jacobian_error_array.loc[row, col] = cov_array.std()/(np.sqrt(effective_samples))

            if verbose and (col == row):               
                plt.figure(figsize=[5, 2])
                plt.title(f"d {row}/d {col} "+" derivative time evolution")

                smoothed_array = np.convolve(-cov_array, derivative_smoothing_filter, mode="same")
                plt.plot(-cov_array, label="raw", alpha=0.1)
                plt.plot(smoothed_array, label="smoothed", alpha=0.7)
                
                plt.ylim([-1.5*np.abs(smoothed_array).max(), 1.5*np.abs(smoothed_array).max()])
                plt.xlabel("Steps")
                plt.legend()
                plt.savefig(f"./{run_folder}/DerivativePics/{row}-{col}-derivative.png", bbox_inches='tight')

                plt.close()

        if verbose:   
            plt.figure(figsize=[5, 2])
            plt.title(row + " pair time evolution")

            smoothed_array = np.convolve(pair_df.loc[:len_traj, row], pair_smoothing_filter, mode="same")
            plt.plot(pair_df.loc[:len_traj, row], label="raw", alpha=0.1)
            plt.plot(smoothed_array, label="smoothed", alpha=0.7)
            plt.ylim([0, 2*smoothed_array.max()])
            plt.xlabel("Steps")
            plt.legend()
            plt.savefig(f"./{run_folder}/PairPics/{row}-pairs.png", bbox_inches='tight')
            plt.close()

    jacobian_array.to_csv(f"./{run_folder}/jacobian.csv")
    jacobian_error_array.to_csv(f"./{run_folder}/jacobian_err.csv")


    return mean_pair_numbers, jacobian_array, jacobian_error_array

