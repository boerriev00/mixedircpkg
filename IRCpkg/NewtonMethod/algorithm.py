import os.path
import pandas as pd
from IRCpkg.NewtonMethod.utils import obtain_pairs_jacobian
import numpy as np


def evaluate_log_loss(FF_df, desired_pairs, para_file, protein_file, run_folder, seed=42, verbose=False, delete_evidence=True, offset_constant=132.5):
    """
    Computes the sum of absolute log differences as a loss function given the parameters specified in the different files.
    :param FF_df: Force field df containing eps2 vals
    :param desired_pairs: the number of pairs desirable
    :param para_file: simulation parameters
    :param protein_file: protein parameters
    :param run_folder: the folder to run the simualtion in
    :param seed: rng seed
    :param verbose: print extra info
    :param delete_evidence: whether to keep the files about like raw IRC trajectories or derivatives
    :return: loss_val, dL_deps, mean_pair_numbers
    loss, gradient and mean pair numbers at the given point.
    """

    mean_pair_numbers, jacobian_array, jacobian_error_array = obtain_pairs_jacobian(FF_df, para_file, protein_file, run_folder, seed=seed,
                                                              verbose=verbose, delete_evidence=delete_evidence)
    print("LOGLOSS CALC")
    print(mean_pair_numbers)
    print(desired_pairs)
    print()

    log_difference = np.log(mean_pair_numbers/desired_pairs.values)
    loss_val = offset_constant*np.mean(np.abs(log_difference))
    dL_dN = offset_constant*np.sign(log_difference)*(1/mean_pair_numbers)*(1/len(desired_pairs.values))

    dL_deps = np.dot(jacobian_array.values.T, dL_dN.values)
    dL_deps_err = (((jacobian_error_array.values.T*dL_dN.values[None,:])**2).sum(axis=-1))**(1/2)
    
    dL_deps = pd.DataFrame(dL_deps, index=jacobian_array.index)
    dL_deps_err = pd.DataFrame(dL_deps_err, index=jacobian_array.index) 

    return loss_val, dL_deps, dL_deps_err, mean_pair_numbers

def evaluate_mse_loss(FF_df, desired_pairs, para_file, protein_file, run_folder, seed=42, verbose=False, delete_evidence=True,  offset_constant=0.0000154):
    """
    Computes the sum squared error as a loss function given the parameters specified in the different files.
    :param FF_df: Force field df containing eps2 vals
    :param desired_pairs: the number of pairs desirable
    :param para_file: simulation parameters
    :param protein_file: protein parameters
    :param run_folder: the folder to run the simualtion in
    :param seed: rng seed
    :param verbose: print extra info
    :param delete_evidence: whether to keep the files about like raw IRC trajectories or derivatives
    :return: loss_val, dL_deps, mean_pair_numbers
    loss, gradient and mean pair numbers at the given point.
    """

    mean_pair_numbers, jacobian_array, jacobian_error_array = obtain_pairs_jacobian(FF_df, para_file, protein_file, run_folder, seed=seed,
                                                              verbose=verbose, delete_evidence=delete_evidence)
    print("MSELOSS CALC")
    print(mean_pair_numbers)
    print(desired_pairs)
    print()

    difference = mean_pair_numbers - desired_pairs.values
    loss_val = offset_constant*np.mean((difference)**2)
    dL_dN = 2*offset_constant*(difference)/len(desired_pairs.values)

    dL_deps = np.dot(jacobian_array.T.values, dL_dN.values)
    dL_deps_err = (((jacobian_error_array.T.values*dL_dN.values[None,:])**2).sum(axis=-1))**(1/2)
    
    dL_deps = pd.DataFrame(dL_deps, index=jacobian_array.index)
    dL_deps_err = pd.DataFrame(dL_deps_err, index=jacobian_array.index) 

    return loss_val, dL_deps, dL_deps_err, mean_pair_numbers


def eval_loss(FF_df, desired_pairs, para_file, protein_file, run_folder, seed=42, verbose=False, loss_func="MSE", delete_evidence=True):
    if loss_func == "LOG":
        return evaluate_log_loss(FF_df, desired_pairs, para_file, protein_file, run_folder, seed=seed, verbose=verbose, delete_evidence=delete_evidence)
    elif loss_func == "MSE":
        return evaluate_mse_loss(FF_df, desired_pairs, para_file, protein_file, run_folder, seed=seed, verbose=verbose, delete_evidence=delete_evidence)
    else:
        print("Please specify a nice loss func from the algorithm.py file")



def newton_iteration(FF_df, desired_pairs, para_file, protein_file, iteration_folder, seed=42, verbose=True, wolfe_c = 0.001,
                     max_it=100, max_ls_it=10, max_newton_update_norm = 10., linesearch_algo="armijo", loss_func="MSE"):
    """
    performs the newton iteration search.
    :param FF_df: the original FF direction
    :param desired_pairs: the number of pairs desirable
    :param para_file: simulation parameters
    :param protein_file: protein parameters
    :param run_folder: the folder to run the simualtion in
    :param iteration_folder: folder to run the step in
    :param seed: rng seed
    :param verbose: print extra info
    :param wolfe_c: constant c for the wolfe conditions (ie. stopping if phi(alpha_k) <= phi(0) + c*alpha_k phi'(0)
    :param max_it: max number of iterations
    :param max_newton_update: the maximum abs value of any update value in the newton update vector
    :param linesearch_algo: {"armijo", "interpolative"} which linesearch algo to use.
    :return: Nothing. (but creates a lot of files and folders. ^^
    """

    if not os.path.exists(iteration_folder):
        os.mkdir(iteration_folder)
    os.chdir(iteration_folder)

    run_folder = f"newton_iteration-{0}"
    loss, dL_deps, dL_deps_err, mean_pair_numbers = eval_loss(FF_df, desired_pairs, para_file, protein_file, run_folder,
                                               seed=seed, verbose=verbose, loss_func=loss_func)

    for i in range(1,max_it + 1):
        run_folder = f"newton_iteration-{i}"
        newton_direction = -(loss)/(dL_deps.values[:,0])
        if np.linalg.norm(newton_direction) > max_newton_update_norm:
            newton_direction = max_newton_update_norm*newton_direction/ np.linalg.norm(newton_direction)
        FF_df["Measured Pairs"] = mean_pair_numbers.values
        FF_df.to_csv(f"./newton_iteration-{i}-FF_df.csv")

        print(f"Newton Iteration {i}")
        print("Current EPS")
        print(FF_df["Eps_AB"])
        print()
        print("Gradient")
        print(dL_deps)
        print()
        print("Gradient Err estimate")
        print(dL_deps_err)
        print()
        print("Newton Direction")
        print(newton_direction)
        print()
        
        print(f"loss before linesearch:" + str(loss))

        if linesearch_algo == "armijo":
            alpha, loss, dL_deps, dL_deps_err, mean_pair_numbers = perform_armijo_linesearch(FF_df, newton_direction, loss,
                                                                                dL_deps, para_file, protein_file,
                                                                                run_folder, seed=seed, verbose=verbose,
                                                                                wolfe_c=wolfe_c, max_it=max_ls_it,
                                                                                loss_func=loss_func)

        elif linesearch_algo == "interpolation":
            alpha, loss, dL_deps, dL_deps_err, mean_pair_numbers = perform_interpolation_linesearch(FF_df, newton_direction, loss,
                                                                                       dL_deps, para_file, protein_file,
                                                                                       run_folder, seed=seed,
                                                                                       verbose=verbose, wolfe_c=wolfe_c,
                                                                                       max_it=max_ls_it, loss_func=loss_func)
        else:
            print("Please select a viable linesearch algorithm from {'armijo', 'interpolation'}")
        print(f"Final Alpha: {alpha}")
        print()

        FF_df["Eps_AB"] = FF_df["Eps_AB"] + alpha*newton_direction

    os.chdir("..")

def gradient_descent_iteration(FF_df, desired_pairs, para_file, protein_file, iteration_folder, seed=42, verbose=True, wolfe_c = 0.01,
                     max_it=100, max_ls_it=10, max_descent_update_norm = 10., linesearch_algo="armijo", loss_func="MSE", descent_fraction=0.01):
    """
    performs the newton iteration search.
    :param FF_df: the original FF direction
    :param desired_pairs: the number of pairs desirable
    :param para_file: simulation parameters
    :param protein_file: protein parameters
    :param run_folder: the folder to run the simualtion in
    :param iteration_folder: folder to run the step in
    :param seed: rng seed
    :param verbose: print extra info
    :param wolfe_c: constant c for the wolfe conditions (ie. stopping if phi(alpha_k) <= phi(0) + c*alpha_k phi'(0)
    :param max_it: max number of iterations
    :param max_newton_update: the maximum abs value of any update value in the newton update vector
    :param linesearch_algo: {"armijo", "interpolative"} which linesearch algo to use.
    :return: Nothing. (but creates a lot of files and folders. ^^
    """

    if not os.path.exists(iteration_folder):
        os.mkdir(iteration_folder)
    os.chdir(iteration_folder)

    run_folder = f"descent_iteration-{0}"
    loss, dL_deps, dL_deps_err, mean_pair_numbers = eval_loss(FF_df, desired_pairs, para_file, protein_file, run_folder,
                                               seed=seed, verbose=verbose, loss_func=loss_func)

    for i in range(1,max_it + 1):
        run_folder = f"descent_iteration-{i}"
        descent_direction = -descent_fraction * dL_deps.values[:,0]

        if np.linalg.norm(descent_direction) > max_descent_update_norm:
            descent_direction = max_descent_update_norm*descent_direction/ np.linalg.norm(descent_direction)

        FF_df["Measured Pairs"] = mean_pair_numbers.values
        FF_df.to_csv(f"./descent_iteration-{i}-FF_df.csv")

        print(f"Descent Iteration {i}")
        print("Current EPS")
        print(FF_df["Eps_AB"])
        print()
        print("Gradient")
        print(dL_deps)
        print()
        print("Gradient Err estimate")
        print(dL_deps_err)
        print()
        print("Descent Direction")
        print(descent_direction)
        print()

        print(f"loss before linesearch:" + str(loss))

        if linesearch_algo == "armijo":
            alpha, loss, dL_deps, dL_deps_err, mean_pair_numbers = perform_armijo_linesearch(FF_df, descent_direction, loss,
                                                                                dL_deps, para_file, protein_file,
                                                                                run_folder, seed=seed, verbose=verbose,
                                                                                wolfe_c=wolfe_c, max_it=max_ls_it,
                                                                                loss_func=loss_func)

        elif linesearch_algo == "interpolation":
            alpha, loss, dL_deps, dL_deps_err, mean_pair_numbers = perform_interpolation_linesearch(FF_df, descent_direction, loss,
                                                                                       dL_deps, para_file, protein_file,
                                                                                       run_folder, seed=seed,
                                                                                       verbose=verbose, wolfe_c=wolfe_c,
                                                                                       max_it=max_ls_it, loss_func=loss_func)
        else:
            print("Please select a viable linesearch algorithm from {'armijo', 'interpolation'}")
        print(f"Final Alpha: {alpha}")
        print()

        FF_df["Eps_AB"] = FF_df["Eps_AB"] + alpha*descent_direction

    os.chdir("..")


def perform_interpolation_linesearch(FF_df, newton_direction, loss_0, grad_0, para_file, protein_file, iteration_folder, seed=42,
                       verbose=False, wolfe_c = 0.01, max_it=10, loss_func="MSE"):
    """
    Performs an interpolative backtracking linesearch along the newton direction.
    this is taken from Nocedal/Wright - Numerical Optimization - 2nd edition - p.59
    We herein compute the minimizer of the cubic interpolation polynomial fit to the func and derivative vals.
    That will be used to obtain an estiamte for the next iteration alpha.
    :param FF_df: the original FF direction
    :param newton_direction: the direction to search along (must have the same index (or at lesat ordering as in FF_df)
    :param loss_0: The original loss for alpha=0
    :param grad_0: The gradient there
    :param para_file: parameter file for running simulations
    :param protein_file: file specifying info about the proteins
    :param iteration_folder: the folder to run the linesearch in
    :param seed: rng seed
    :param verbose: whether to print extra info and
    :param wolfe_c: constant c for the wolfe conditions (ie. stopping if phi(alpha_k) <= phi(0) + c*alpha_k phi'(0)
    :param reducing_alpha: the factor by which alpha will be reduced between iterations
    :param max_it: max number of iterations
    :return: this_alpha, this_loss, this_gradient, mean_pair_numbers
    the alpha, loss, gradient and pair numbers at the final iteration
    """
    last_alpha = 0.
    last_loss = loss_0
    last_gradient = grad_0
    # phi is the funciton L(\alpha)
    zero_dphi_dalpha =  np.dot(grad_0.values.T, newton_direction)
    zero_loss = loss_0
    last_dphi_dalpha = np.dot(last_gradient.values.T, newton_direction)

    this_alpha = 1.

    if not os.path.exists(iteration_folder):
        os.mkdir(iteration_folder)
    os.chdir(iteration_folder)

    for i in range(max_it+1):
        print(f"LineSearch-Iteration-{i}-alpha-{this_alpha}")

        run_folder = f"iteration-{i+1}"
        run_FF_df = FF_df.copy()
        run_FF_df["Eps_AB"] = run_FF_df["Eps_AB"] + this_alpha*newton_direction

        this_loss, this_gradient, this_gradient_err, mean_pair_numbers = eval_loss(run_FF_df, run_FF_df["N_pairs"], para_file, protein_file, run_folder, seed=seed,
                        verbose=verbose,loss_func=loss_func)
        print(f"Achieved Loss-{this_loss}")
        print()

        wolfe_condition = (this_loss <= zero_loss + wolfe_c*this_alpha*zero_dphi_dalpha)
        if wolfe_condition:
            break

        this_dphi_dalpha = np.dot(this_gradient.values.T, newton_direction)
        # compute the minimizer of the cubic interpolation of the data if the wolfe conditions are not met
        d1 = last_dphi_dalpha + this_dphi_dalpha - 3*(last_loss - this_loss)/(last_alpha - this_alpha)
        d2 = np.sign(this_alpha - last_alpha)*(d1**2 - last_dphi_dalpha*this_dphi_dalpha)**(1/2)
        bracket = (this_dphi_dalpha + d2 - d1)/(this_dphi_dalpha - last_dphi_dalpha + 2*d2)
        trial_alpha = this_alpha - (this_alpha - last_alpha)*bracket

        # update the different paras and all
        # its not the greatest scheme, but for simplicity, just keep the point with the lower loss and disregard the
        # other point
        if last_loss >= this_loss:
            last_loss, last_gradient, last_dphi_dalpha, last_alpha = this_loss, this_gradient, this_dphi_dalpha, this_alpha

        this_alpha = trial_alpha

    # if we hit max iterations, just go (1/20)th of the newton direction...
    if i == max_it:
        this_alpha = 0.1 * 1/np.sqrt(np.dot(newton_direction, newton_direction))
        run_folder = f"iteration-{i + 1}"
        run_FF_df = FF_df.copy()
        run_FF_df["Eps_AB"] = run_FF_df["Eps_AB"] + this_alpha * newton_direction
        this_loss, this_gradient, this_gradient_err, mean_pair_numbers = eval_loss(run_FF_df, run_FF_df["N_pairs"], para_file, protein_file, run_folder,
                                                    seed=seed, verbose=verbose,loss_func=loss_func)

    os.chdir("..")
    return this_alpha, this_loss, this_gradient, this_gradient_err, mean_pair_numbers


def perform_armijo_linesearch(FF_df, newton_direction, loss_0, grad_0, para_file, protein_file, iteration_folder, seed=42,
                       verbose=False, wolfe_c = 0.01, reducing_alpha=0.25, max_it=10, loss_func="MSE"):
    """
    Performs an armijo backtracking linesearch along the newton direction
    :param FF_df: the original FF direction
    :param newton_direction: the direction to search along (must have the same index (or at lesat ordering as in FF_df)
    :param loss_0: The original loss for alpha=0
    :param grad_0: The gradient there
    :param para_file: parameter file for running simulations
    :param protein_file: file specifying info about the proteins
    :param iteration_folder: the folder to run the linesearch in
    :param seed: rng seed
    :param verbose: whether to print extra info and
    :param wolfe_c: constant c for the wolfe conditions (ie. stopping if phi(alpha_k) <= phi(0) + c*alpha_k phi'(0)
    :param reducing_alpha: the factor by which alpha will be reduced between iterations
    :param max_it: max number of iterations
    :return: this_alpha, this_loss, this_gradient, mean_pair_numbers
    the alpha, loss, gradient and pair numbers at the final iteration
    """

    last_loss = loss_0
    last_gradient = grad_0
    # phi is the funciton L(\alpha)
    zero_dphi_dalpha = np.dot(grad_0.values.T, newton_direction)
    zero_loss = loss_0
    last_dphi_dalpha = np.dot(last_gradient.values.T, newton_direction)

    this_alpha = 1.

    if not os.path.exists(iteration_folder):
        os.mkdir(iteration_folder)
    os.chdir(iteration_folder)

    for i in range(1,max_it+1):
        print(f"LineSearch-Iteration-{i}-alpha-{this_alpha}")

        run_folder = f"iteration-{i}"
        run_FF_df = FF_df.copy()
        run_FF_df["Eps_AB"] = run_FF_df["Eps_AB"] + this_alpha*newton_direction
        this_loss, this_gradient, this_gradient_err, mean_pair_numbers = eval_loss(run_FF_df, run_FF_df["N_pairs"], para_file, protein_file, run_folder, seed=seed,
                        verbose=verbose, loss_func=loss_func)

        print(f"Achieved Loss-{this_loss}")
        print()

        wolfe_condition = (this_loss <= zero_loss + wolfe_c*this_alpha*zero_dphi_dalpha)
        if wolfe_condition:
            break

        this_alpha = this_alpha*reducing_alpha
        last_loss = this_loss

    # if we hit max iterations, just go (1/10)th of the newton direction...
    if i == max_it:
        this_alpha = 0.1 * 1/np.sqrt(np.dot(newton_direction, newton_direction))
        run_FF_df = FF_df.copy()
        run_FF_df["Eps_AB"] = run_FF_df["Eps_AB"] + this_alpha * newton_direction
        this_loss, this_gradient, this_gradient_err, mean_pair_numbers = eval_loss(run_FF_df, run_FF_df["N_pairs"], para_file, protein_file, run_folder,
                                                    seed=seed,
                                                    verbose=verbose,loss_func=loss_func)

    os.chdir("..")
    return this_alpha, this_loss, this_gradient, this_gradient_err, mean_pair_numbers

