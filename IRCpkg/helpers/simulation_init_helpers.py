import openmm.openmm as omm
import openmm.unit
from openmm import vec3
import openmm.app as oap
import numpy as np

def setup_system(periodic_vectors: np.ndarray,  particle_types: np.ndarray
                , particle_mass = 30.*(omm.unit.kilo*omm.unit.dalton), verbose=True):

    unique_particle_types, no_particles_per_type = np.unique(particle_types, return_counts=True)

    element_idx = 200
    element_dict = {}
    element_name_dict = {}

    p_ele = oap.Element(200, "PX", "X", particle_mass)

    
    for tmp_particle_type in unique_particle_types:
        element_dict[tmp_particle_type] = p_ele
        element_idx += 1


    system = omm.System()
    topology = oap.Topology()

    tot_chain = topology.addChain(id=str(1))

    residue_dict = {}
    residue_id_dict = {}
    i = 1
    for tmp_particle_type in unique_particle_types:
        particle_type_name = str(tmp_particle_type)

        residue_dict[particle_type_name] = topology.addResidue(particle_type_name, tot_chain, id=str(i))
        residue_id_dict[particle_type_name] = i
        i+=1

    particle_idx_dict = {}

    for particle_type_idx in range(len(unique_particle_types)):
        tmp_particle_type = unique_particle_types[particle_type_idx]

        tmp_no_particles = no_particles_per_type[particle_type_idx]
        tmp_type_element = element_dict[tmp_particle_type]
        tmp_type_residue = residue_dict[tmp_particle_type]

        tmp_particle_idxs = np.zeros(tmp_no_particles)
        for i in range(tmp_no_particles):
            tmp_particle_idxs[i] = system.addParticle(tmp_type_element.mass)
            topology.addAtom(f'{str(tmp_particle_type)}{i}', tmp_type_element, residue=tmp_type_residue)

        particle_idx_dict[tmp_particle_type] = tmp_particle_idxs


    e_x = vec3.Vec3(periodic_vectors[0,0],periodic_vectors[0,1],periodic_vectors[0,2])
    e_y = vec3.Vec3(periodic_vectors[1,0],periodic_vectors[1,1],periodic_vectors[1,2])
    e_z = vec3.Vec3(periodic_vectors[2,0],periodic_vectors[2,1],periodic_vectors[2,2])

    system.setDefaultPeriodicBoxVectors(e_x, e_y, e_z)

    if verbose:
        for particle_type_idx in range(len(unique_particle_types)):
            tmp_particle_type = unique_particle_types[particle_type_idx]

            tmp_type_element = element_dict[tmp_particle_type]
            tmp_type_residue = residue_dict[tmp_particle_type]

            print(tmp_type_element)
            print(tmp_type_residue, f'with id:{tmp_type_residue.id}')
            print()

    return system, topology, particle_idx_dict, residue_id_dict

def obtain_particle_grid(N: int, L: openmm.unit.Quantity, rng: np.random.Generator =np.random.default_rng(seed=42), unit=omm.unit.nanometer):
    """
        This will return a numpy array of [N,3] particle coordinates on a grid, randomly shuffled along the N-axis.
        Also, it will give the sidelength of the box.

        :param N:   total particle number
        :param L:  box-length (preferably in nm)
        :param rng: numpy random number generator instance
        :return:
        init_positions: np.ndarray of size [N,3] particle coordinates
        L: float, Box sidelength
        """

    theo_spac = (0.99*L) / (N ** (1 / 3))
    Lx_spacing = theo_spac
    Ly_spacing = theo_spac

    # setup the number of particles per axis, z gets an extra row, such that we can reach N for noninteger N**(1/3)
    Nx = int(np.floor(L / Lx_spacing))
    Ny = int(np.floor(L / Ly_spacing))
    Nz = int(np.ceil(N / (Nx * Ny)))

    # adjust the Z spacing
    Lz_spacing = (0.99*L) / (Nz)

    # create initial position array and fill it
    init_positions = np.zeros((N, 3))

    i = 0
    for x in range(Nx):
        for y in range(Ny):
            for z in range(Nz):
                if i >= N:
                    break
                init_positions[i, 0] = x * Lx_spacing.value_in_unit(unit)
                init_positions[i, 1] = y * Ly_spacing.value_in_unit(unit)
                init_positions[i, 2] = z * Lz_spacing.value_in_unit(unit)

                i += 1

    # shuffle the positions and return the box length and positions
    rng.shuffle(init_positions, axis=0)
    return init_positions

def numpy_to_vec3d(pos_array, unit=omm.unit.nanometer):
    """
    Converterts between numpy and the openMM vec3D class
    :param pos_array: a 2D array of shape [N,3] containing positions measured in nanometer
    :return: a omm Quantity containing vec3d objects with the same positions (and unit, unit)
    """
    N = pos_array.shape[0]
    vec3d_array = []
    for i in range(N):
        tmp_ele = pos_array[i]
        vec3d_array.append(vec3.Vec3(tmp_ele[0], tmp_ele[1], tmp_ele[2]))
    vec3d_array = omm.unit.quantity.Quantity(vec3d_array, unit)
    return vec3d_array


def create_custom_nonbonded_force(expression : str, para_dict : dict, system : omm.System,
                             interaction_group_idcsA : np.ndarray =None, interaction_group_idcsB : np.ndarray=None,
                             pbcs=True, cutoff=None, switch_factor= None):
    """
    This will create a openMM CustomNonbondedForce according to the expression specified, with the values for the
    parameters specified in the para_dict as pairs of "parameter_name : value". It will also add the force to the
    simulation given and apply it to the particles given in the particle indices in interaction group A and B.
    Optionally, pbcs are applied, a cutoff distance is used as well as a switch function, which is applied after
    cutoff_distance*switch_factor and smoothly transitions to 0.
    Please supply all physical quantities as an omm.quantity.Quantity with suitable units.
    :param expression: The expression of the force together with parameters. e.g. "0.5*k*r^2"
    :param para_dict: a dict of parameter - value pairs, e.g. {"k": 1.*omm.unit.angstrom}
    :param system: the openMM system to add the force to
    :param interaction_group_idcsA: (optional), an array containing atom indices to apply the force to
    :param interaction_group_idcsB: (optional), an array containing atom indices to apply the force to such that the
                                    force is applied between all pairs of particles in A and B
    :param pbcs:                    whether to use pbcs
    :param cutoff:                  cutoff distance. None for no cutoff
    :param switch_factor:  float    factor after which a switching function is applied
                                    (at cutoff_distance * switching_factor) None for no switch function
    :return: force, interact_idx
    the openMM force object as well as the interaction group index if interaction groups are used
    """

    # create a force with the expression and add its parameters to it
    force = omm.CustomNonbondedForce(expression)
    for name in para_dict:
        force.addGlobalParameter(name, para_dict[name])

    # apply pbcs if needed and use a cutoff if no pbcs but cutoff
    if pbcs:
        force.setNonbondedMethod(2)
    elif cutoff is not None:
        force.setNonbondedMethod(1)

    # set cutoff dictance and switching function
    if cutoff is not None:
        force.setCutoffDistance(cutoff)

    if switch_factor is not None:
        force.setUseSwitchingFunction(True)
        force.setSwitchingDistance(switch_factor*cutoff)

    # add particles and select interaction group
    for _ in range(system.getNumParticles()):
        force.addParticle()

    if interaction_group_idcsA is not None and interaction_group_idcsB is not None:
        interact_idx = force.addInteractionGroup(interaction_group_idcsA, interaction_group_idcsB)
        return force, interact_idx

    else:
        return force

