import numba
import numpy as np
import warnings
warnings.filterwarnings("ignore")
from openmm.unit import kilojoule_per_mole, meter, nano
from numba import float32, jit


@jit(nopython=False)
def obtain_string(key_list, val_dict, step):
    ret_string = f"{step}"
    for key in key_list:
        ret_string = ret_string + f",{val_dict[key]}"

    ret_string = ret_string + "\n"

    return ret_string


class DerivativeReporter(object):
    def __init__(self, file, reportInterval_steps, der_dict):
        """
        This Reporter saves the calculated Derivatives.
        :param file: output text file.
        :param reportInterval_steps: every steps, the derivatives will be saved
        :param irc_dict: dictionary containing pairs of "Irc pair name" : "Irc pair parameter name", s.t. the derivative
        corresponding to some Irc pair will be the "Irc pair parameter name" derivative
        """

        self._out = open(file, 'w')
        self.reportInterval_steps = reportInterval_steps
        self.der_dict = numba.typed.Dict.empty(numba.types.unicode_type, numba.types.unicode_type)

        # create a list of the irc pair names and keys and write a header for the file
        self._out.write(f"Step")
        der_dict_keys = []

        for key in der_dict.keys():
            der_dict_keys.append(str(der_dict[key]))
            self.der_dict[str(key)] = str(der_dict[key])
            self._out.write(f",{key}")

        self.der_dict_keys = np.asarray(der_dict_keys, dtype=np.dtype("U20"))
        self._out.write(f"\n")

    def __del__(self):
        self._out.close()

    def describeNextReport(self, simulation):
        steps = self.reportInterval_steps - simulation.currentStep % self.reportInterval_steps
        return (steps, False, False, False, False, None)

    def report(self, simulation, state):
        # report by getting the Derivatives, turning them into a string and writing that to file
        step = simulation.currentStep
        val_dict = simulation.context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives()
        out_string = obtain_string(self.der_dict_keys, val_dict, step)
        self._out.write(out_string)

class DerivativeIRCReporter(object):
    def __init__(self, file, reportInterval_steps, irc_dict):
        """
        This Reporter saves the calculated Derivatives, same as above.
        But it is meant for reporting IRC values through the derivative hack.
        :param file: output text file.
        :param reportInterval_steps: every steps, the derivatives will be saved
        :param irc_dict: dictionary containing pairs of "Irc pair name" : "Irc pair parameter name", s.t. the derivative
        corresponding to some Irc pair will be the "Irc pair parameter name" derivative
        """
        self._out = open(file, 'w')
        self.reportInterval_steps = int(reportInterval_steps)
        self.irc_dict = numba.typed.Dict.empty(numba.types.unicode_type, numba.types.unicode_type)

        # create a list of the irc pair names and keys and write a header for the file
        self._out.write(f"Step")
        irc_dict_keys = []

        for key in irc_dict.keys():
            irc_dict_keys.append(str(irc_dict[key]))
            self.irc_dict[str(key)] = str(irc_dict[key])
            self._out.write(f",{key}")

        self.irc_dict_keys = np.asarray(irc_dict_keys, dtype=np.dtype("U20"))
        self._out.write(f"\n")

    def __del__(self):
        self._out.close()

    def describeNextReport(self, simulation):
        steps = self.reportInterval_steps - simulation.currentStep % self.reportInterval_steps
        return (steps, False, False, False, False, None)

    def report(self, simulation, state):
        # report by getting the Derivatives, turning them into a string and writing that to file
        step = simulation.currentStep
        val_dict = simulation.context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives()
        out_string = obtain_string(self.irc_dict_keys, val_dict, step)
        self._out.write(out_string)


# this is a reporter copied from the wiki meant to report forces
# source: http://docs.openmm.org/latest/userguide/application/04_advanced_sim_examples.html
# it is simply meant as an example of how such a reporter might work.

class ForceReporter(object):
    def __init__(self, file, reportInterval):
        self._out = open(file, 'w')
        self._reportInterval = reportInterval

    def __del__(self):
        self._out.close()

    def describeNextReport(self, simulation):
        steps = self._reportInterval - simulation.currentStep%self._reportInterval
        return (steps, False, False, True, False, None)

    def report(self, simulation, state):
        forces = state.getForces().value_in_unit(kilojoule_per_mole/(nano*meter))
        for f in forces:
            self._out.write('%g %g %g\n' % (f[0], f[1], f[2]))
