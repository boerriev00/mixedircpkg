from IRCpkg.getBasePath import getBasePath
import subprocess
import os

folder = "run"
if not os.path.exists(folder):
    os.mkdir(folder)

irc_path = getBasePath()
run_string = (f"python -u {irc_path}/run_mixed_simulation.py --parameterFile defaultParaFile.csv "
              f"--proteinFile exampleProteinDF.csv --forceFieldFile exampleFF_DF.csv "
              f"--folder {folder} --calcRDF > ./{folder}/plainSim.txt")

subprocess.run(run_string, shell=True)