import os
import openmm as omm
import pandas as pd
import argparse
import pickle as pkl
from IRCpkg.mixed_system_simulation import mixed_system_simulation
from IRCpkg.tools.compute_N_IRC_RDF import compute_N_IRC_RDF
nanosecond = omm.unit.nano*omm.unit.second
picosecond = omm.unit.pico*omm.unit.second
microsecond = omm.unit.micro*omm.unit.second



default_para_location = os.path.dirname(os.path.abspath(__file__)) + "/defaultParams/defaultParaFile.csv"

parser = argparse.ArgumentParser()
parser.add_argument("--folder", type=str, required=True, help="Folder to run the simulation in (must exist already).")
parser.add_argument("--forceFieldFile", required=True, type=str,
                    help="CSV file containing the ForceField parameters for this simulation. A template for this can be found in "
                         + os.path.dirname(os.path.abspath(__file__))+ "/defaultParams")
parser.add_argument("--proteinFile", required=True, type=str,
                    help="CSV file containing the Protein Numbers parameters for this simulation. A template for this can be found in "
                         + os.path.dirname(os.path.abspath(__file__))+ "/defaultParams")
parser.add_argument("--parameterFile", type=str, default=default_para_location,
                    help="CSV file containing the parameters for this simulation. A template for this can be found in "
                         + os.path.dirname(os.path.abspath(__file__))+ "/defaultParams")
parser.add_argument("--seed", type=int, default=42, help="RNG Seed")
parser.add_argument("--runSim", type=bool, default=True, help="whether to run the actual simulation")

parser.add_argument("--calcRDF", action='store_true', help="whether to calculate the IRC values")
parser.add_argument("--calcDerivative", action='store_true',
                    help="whether to calculate the eps_2_AB derivative as well")
parser.add_argument("--discardTrajectory", action='store_true',
                    help="whether to NOT store the trajectory during the simulation")
parser.add_argument("--disableInSituReporting", action='store_true',
                    help="whether to NOT calculate the irc values per timestep during the simulation")


args = parser.parse_args()

# non negotiable paras
folder = args.folder
protein_file = args.proteinFile
force_field_file = args.forceFieldFile

# default paras
seed = args.seed
para_file = args.parameterFile

# what to run paras
run_simulation_too = args.runSim
calcDerivative = args.calcDerivative
disable_in_situ_reporting = args.disableInSituReporting
discard_trajectory = args.discardTrajectory
calc_rdf = args.calcRDF

# run parameters
para_df = pd.read_csv(para_file, index_col=0).iloc[:,0]
protein_df = pd.read_csv(protein_file, index_col=0)
force_field_df = pd.read_csv(force_field_file, index_col=0)

# print the input dfs s.t. results and paras are stored together
print(para_df)
print()
print(protein_df)
print()
print(force_field_df)
print()

# time parameters
equilibrate_T = float(para_df["equilibration time [mu s]"]) * microsecond
production_T = float(para_df["production time [mu s]"]) * microsecond
sampling_dt = float(para_df["sampling timestep [ns]"]) * nanosecond
integration_dt = float(para_df["integration timestep [ps]"]) * picosecond
damping_gamma = float(para_df["damping constant [1/ps]"]) * (1/picosecond)

# run settings
volume = para_df["volume [nm**3]"] * (omm.unit.nano*omm.unit.meter)**3
simulation_alpha = float(para_df["simulation alpha"])

# create folder if necessary
if not os.path.exists(folder):
    os.mkdir(folder)
os.chdir(folder)

# calc number of steps inbetween saving
saving_freq = int(sampling_dt/integration_dt)

# and run the Simulation!
sim_filename, residue_id_dict = mixed_system_simulation(protein_df, force_field_df, volume,
    T = 298.*omm.unit.kelvin, particle_mass =  30.*(omm.unit.kilo * omm.unit.dalton),
    prod_T = production_T, eq_T = equilibrate_T, saving_freq = saving_freq,
    dt = integration_dt, gamma = damping_gamma,
    alpha = simulation_alpha, calc_eps_derivative = calcDerivative, disable_in_situ_reporting = disable_in_situ_reporting,
    discard_trajectory = discard_trajectory, seed = seed)

# save the residue id dict!
print(residue_id_dict)
print("DONE!")
with open(f"./output_{sim_filename}.pkl", "wb") as file:
    pkl.dump(residue_id_dict, file)

### calc the rdfs of pairs in force_field_df in case of "calcRDF"
if calc_rdf:
    rdf_folder = "rdfPics"
    if not os.path.exists(rdf_folder):
        os.mkdir(rdf_folder)
    for i, row in force_field_df.iterrows():
        proteinA = row['ProteinA']
        proteinB = row['ProteinB']

        selection_A = f"resid {residue_id_dict[proteinA]}"
        selection_B = f"resid {residue_id_dict[proteinB]}"

        r_m_A = protein_df[protein_df["name"] == proteinA]["r_m [A]"].astype(float)
        r_m_B = protein_df[protein_df["name"] == proteinB]["r_m [A]"].astype(float)

        r_IRC = simulation_alpha*(r_m_A.values[0]+r_m_B.values[0])

        compute_N_IRC_RDF(selection_A, selection_B, prefix=".", sim_filename=sim_filename, r_IRC=r_IRC,
                          out_filename=f"{rdf_folder}/{proteinA}-{proteinB}")
