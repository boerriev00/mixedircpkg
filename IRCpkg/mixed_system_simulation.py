import numpy as np
import openmm
from openmm import openmm as omm
import openmm.app as oap
from IRCpkg.helpers.simulation_init_helpers import setup_system, obtain_particle_grid, numpy_to_vec3d, \
    create_custom_nonbonded_force
from IRCpkg.helpers.extra_reporters import DerivativeReporter, DerivativeIRCReporter

from mdtraj.reporters import XTCReporter
from datetime import date
from time import time

def mixed_system_simulation(p_list_df, ff_df, volume,
    alpha: float = 1.3, T = 298.*omm.unit.kelvin, particle_mass =  30.*(omm.unit.kilo * omm.unit.dalton), eps_1 = 10.,
    prod_T = 20. *(omm.unit.micro*omm.unit.second), eq_T = 20. *(omm.unit.micro*omm.unit.second),
    saving_freq : int = 50, dt = 20.*(omm.unit.pico*omm.unit.second), gamma = 850. *((omm.unit.pico*omm.unit.second)**(-1)),
    calc_eps_derivative : bool = False, disable_in_situ_reporting : bool = False, discard_trajectory : bool = False,
    seed : int = 42, run_platform : str = 'CUDA'
    ):
    """
    This will run a simulation of the Proteins specified in p_list_df at the volume given in "volume",
     according to the forcefield specified in ff_df.
    Run time parameters are given as optional parameters, optionally, the eps_2 parameter derivative will also be reported
    and pair Numbers calculated and reported during the simulation.
    :param p_list_df: pd.DataFrame containing the required information about the proteins.
    Must have the following colums: "name","r_m [A]","count", the name, radius r_m and raw protein number
    :param ff_df: pd.DataFrame specifying FF-eps_2-parameters.
    Must contain the following columns. ProteinA,ProteinB,Eps_AB- Names of Proteins A/B and a Eps_2_AB value (in kbT)
    Names must be the same as in p_list_df
    :param volume: openMM.quantity (prefered unit - nm**3), Volume of the simulation Box
    :param alpha: float (>= 1. !) - ratio between r_m and r_M
    :param T: openMM.quantity (prefered unit - K), temperature
    :param eps_1: float - value of the repulsion parameter eps_1 in kbT
    :param particle_mass: openMM.quantity (prefered unit - kDa), mass of the particles
    :param prod_T: openMM.quantity (prefered unit - mu s), production time
    :param eq_T: openMM.quantity (prefered unit - mu s), equilibration time
    :param saving_freq: int, number of steps inbetween saved simulation steps
    :param dt: openMM.quantity (prefered unit - ps), integration timestep
    :param gamma: openMM.quantity (prefered unit - ps^{-1}), langevin damping constant
    :param calc_eps_derivative: bool, whether to calculate the derivative dH/deps_2 for the different pairs specified in
    ff_df during the simulation run.
    :param disable_in_situ_reporting: bool, whether to calculate pair numbers during the simulation run.
    :param discard_trajectory: bool, if true, the trajectory will not be saved during the simulation.
    :param seed: int, rng seed
    :param run_platform: str, either "CPU", "CUDA", "OpenCL" (the openMM platforms)
    :return:
    """

    ##### Misc
    # setup rng
    rng = np.random.default_rng(seed=seed)

    # create a filename out of parameters and date
    today = date.today()
    save_file_name = f'sim_epsAB_day{today.day}_mon{today.month}'
    k_B_T = T * omm.unit.BOLTZMANN_CONSTANT_kB * omm.unit.AVOGADRO_CONSTANT_NA


    ##### Set Up System
    # create a list of different particle types
    particle_type_list = []
    p_list_df["count"] = p_list_df["count"].astype(int)
    for i, row in p_list_df.iterrows():
        particle_type_list = particle_type_list + [row["name"]] * row["count"]

    # create box and an equidistant grid of initial particle positions
    tot_no_particles = p_list_df["count"].sum()
    box_len = volume ** (1 / 3)

    particle_init_positions = obtain_particle_grid(tot_no_particles, box_len, rng=rng,
                                                      unit= box_len.unit)
    particle_init_positions = numpy_to_vec3d(particle_init_positions, unit=box_len.unit)
    box_vectors = np.diag((np.ones(3) * box_len))

    # as a Sidenote: particle_init_positions is a shuffled list of points in the grid, such that each particle will get
    # a random position on the grid
    # Fill in the particles, create an openMM system and topology accordingly, also obtain a list of particle indices
    # as well as a dictionary to keep track of wich Proteins got which residue id.
    system, topology, particle_idxs, residue_id_dict = setup_system(box_vectors, particle_type_list,
                                                                    particle_mass=particle_mass,
                                                                    verbose=True)

    n_particle_types = len(p_list_df)

    ##### FORCE FIELD

    force_string = ""
    tabu_func_string = ""

    eps_2_name_to_val_dict = {}
    r_m_name_to_val_dict = {}
    r_M_name_to_val_dict = {}

    tabulated_func_dict = {}
    
    irc_dict = {}
    eps_2_der_dict = {}

    max_r_M = 0.

    # iterate through protein types
    for i, rowA in p_list_df.iterrows():
        for j, rowB in p_list_df.iterrows():

            if i > j:
                continue

            nameA, nameB = rowA["name"], rowB["name"]
            r_m_A, r_m_B = rowA["r_m [A]"] / 10., rowB["r_m [A]"] / 10.

            name_r_m = f"r_m_{nameA}_{nameB}"
            
            
            # setup params
            r_m_AB = r_m_A + r_m_B
            r_M_AB = alpha * r_m_AB

            if max_r_M < r_M_AB:
                max_r_M = r_M_AB
            
            r_m_name_to_val_dict[name_r_m] = r_m_AB

            # find the appropriate entry in the ff df
            selection = ff_df[((ff_df.ProteinA == nameA) & (ff_df.ProteinB == nameB)) | (
                        (ff_df.ProteinB == nameA) & (ff_df.ProteinA == nameB))]

            if len(selection) == 1:
                eps_2_AB = selection["Eps_AB"].values[0] * k_B_T
                print(f"Eps2-{nameA}-{nameB} : {eps_2_AB}")
                name_eps_2 = f"eps_2_{nameA}_{nameB}"
                name_r_M = f"r_M_{nameA}_{nameB}"
                
                eps_2_der_dict[f"{nameB}-{nameA}"] = name_eps_2
                
                eps_2_name_to_val_dict[name_eps_2] = eps_2_AB
                r_M_name_to_val_dict[name_r_M] = r_M_AB

                expression_AB = f"(1-step(r-{name_r_m}))*( (eps_1 + {name_eps_2})*( r/{name_r_m} - 1)^2 - {name_eps_2} ) + " + \
                            (f"( (step(r-{name_r_m}))*(1-step(r-{name_r_M})) )*( -1*{name_eps_2} * ({name_r_M} - r)^2 *"
                             f" ({name_r_M} - 3*{name_r_m} + 2*r) / ({name_r_M} - {name_r_m})^3 )")


                if not disable_in_situ_reporting:
                    irc_para_name = f"irc_para_{nameA}_{nameB}"
                    expression_AB = expression_AB + f" + {irc_para_name}*step({name_r_M} - r) "
                    irc_dict[f"{nameB}-{nameA}"] = irc_para_name
                
            else:
                print(f"Warning - {len(selection)} entries for pair {nameA} {nameB}")
                expression_AB = f"(1-step(r-{name_r_m}))*( (eps_1)*( r/{name_r_m} - 1)^2)"

                            
            delta_func_name = f"delta_func_{nameA}_{nameB}"
            delta_name = f"delta_{nameA}_{nameB}"
            
            delta_func_list = np.zeros((n_particle_types, n_particle_types), dtype="float64")
            delta_func_list[i,j] = 1.
            delta_func_list[j,i] = 1.
            delta_func_list  = (delta_func_list).ravel().tolist()
            tabulated_func_dict[delta_func_name] = omm.Discrete2DFunction(n_particle_types, n_particle_types, delta_func_list)

            expression_AB = f"{delta_name}*({expression_AB})"

            if force_string != "":
                force_string  = force_string + " + " + expression_AB
            else:
                force_string = expression_AB
            
            tabu_func_string = tabu_func_string + " ; " + f"{delta_name}={delta_func_name}(type1, type2)"

    full_force = omm.CustomNonbondedForce(force_string + tabu_func_string)
    full_force.setCutoffDistance(max_r_M*1.01)
    full_force.setNonbondedMethod(2)

    full_force.addGlobalParameter("eps_1", eps_1 * k_B_T)

    for name in r_m_name_to_val_dict:
        full_force.addGlobalParameter(name, r_m_name_to_val_dict[name])
    for name in r_M_name_to_val_dict:
        full_force.addGlobalParameter(name, r_M_name_to_val_dict[name])
    for name in eps_2_name_to_val_dict:
        full_force.addGlobalParameter(name, eps_2_name_to_val_dict[name])
    for name in tabulated_func_dict:
        full_force.addTabulatedFunction(name, tabulated_func_dict[name])

    if not disable_in_situ_reporting:
        for name in irc_dict:
            full_force.addGlobalParameter(irc_dict[name], 0.*k_B_T)
            full_force. addEnergyParameterDerivative(irc_dict[name])

    if calc_eps_derivative:
        for name in eps_2_der_dict:
            full_force. addEnergyParameterDerivative(eps_2_der_dict[name])
    
    full_force.addPerParticleParameter("type")

                
    # add each atom in the simulation to this force
    for atom in topology.atoms():
        atom_residue = atom.residue.name
        selection = p_list_df[p_list_df["name"] == atom_residue]

        # if there is only one val of r_m for this type, add the particle
        if len(selection) == 1.:
            particle_type = int(selection.index[0])
            full_force.addParticle([particle_type])

        if len(selection) != 1.:
            print(f"Warning, unmatched or overmatched r_m for atom{atom} !")

    system.addForce(full_force)

    ##### Run the simulation!

    ### Set up integrator
    # integrator and saving freq
    integrator = omm.LangevinMiddleIntegrator(T, gamma, dt)
    integrator.setRandomNumberSeed(seed=seed)

    # set up some time parameters for the run
    no_steps = int(prod_T / dt)
    eq_steps = int(eq_T / dt)

    print(f'No of steps {no_steps}')
    print(f'No of saved steps {no_steps / saving_freq}')
    print()

    ### Final System setup
    run_platform = omm.Platform.getPlatformByName(run_platform)
    simulation = oap.Simulation(topology, system, integrator=integrator, platform=run_platform)
    simulation.context.setPositions(particle_init_positions)
    simulation.context.setVelocitiesToTemperature(T, seed)

    print("Setup Done!")

    start_time = time()
    simulation.step(eq_steps)
    print("Equilibration Done!")

    # Add all the reporters necessary for the desired output
    simulation.reporters.append(
        oap.statedatareporter.StateDataReporter(f'./{save_file_name}_stdout.txt', saving_freq, step=True,
                                                potentialEnergy=True, temperature=True, totalEnergy=True,
                                                kineticEnergy=True))

    if not discard_trajectory:
        top_reporter = oap.pdbreporter.PDBReporter(f'./{save_file_name}.pdb', saving_freq)
        top_reporter.report(simulation, simulation.context.getState(getPositions=True))

        simulation.reporters.append(XTCReporter(f'./{save_file_name}.xtc', saving_freq))

    if not disable_in_situ_reporting:
        simulation.reporters.append(DerivativeIRCReporter(f'./{save_file_name}-IRC_AB.txt', saving_freq, irc_dict))


    if calc_eps_derivative:
        simulation.reporters.append(DerivativeReporter(f'./{save_file_name}-EPS_AB.txt', saving_freq, eps_2_der_dict))

    # ANND LETS GOOO
    simulation.step(no_steps)

    print(save_file_name, ";", residue_id_dict, ";")
    print(f"elapsed time [s]: {time() - start_time}")

    return save_file_name, residue_id_dict