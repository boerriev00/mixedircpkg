import numpy as np
import MDAnalysis as mda
from MDAnalysis.analysis.rdf import InterRDF
import matplotlib.pyplot as plt

def calc_N_r(bins : np.ndarray, rdf : np.ndarray, rho_B : float):
    """
    Calculates the average number of particles B at any given distance r from the particles A, given the average density
    of particles B, rho_B
    :param bins: np.ndarray of size [n_bins] the rdf bin centers (in Angstrom) (bins must all have equal width)
    :param rdf: np.ndarray of size [n_bins] the actual rdf
    :param rho_B: the density of particles B (in 1/angstrom**3)
    :return: n_r_array,  np.ndarray of size n_bins
    """

    lower_bin_edges = bins - bins[0]
    upper_bin_edges = bins + bins[0]

    bin_volumina = (4/3)*np.pi*(upper_bin_edges**3) - (4/3)*np.pi*(lower_bin_edges**3)

    n_r_array = rdf*rho_B*bin_volumina

    return n_r_array


def compute_N_IRC_RDF(select_A : str, select_B : str, prefix : str, sim_filename: str, r_IRC : float, rdf_factor : float = 2.0,
                      output_rdf : bool = True, check_equilibrium : bool = True, store_counts: bool = False, out_filename : str = None):
    """
    Computes the number of particle pairs within r_IRC selected from the universe stored in the folder "prefix"
    with sim_filename "sim_filename" based on the selection rule select_A and select_B.
    Optionally also computes the rdf until rdf_factor*r_IRC and stores it as a picture in the current directory.
    Further, optionally also computes rdf and IRC using only the first 20% of the trajectory to see whether the IRC is
    in equilibrium.
    :param select_A: String to select atom group A from the Mdanalysis universe
    :param select_B: String to select atom group B from the Mdanalysis universe
    :param prefix:   folder in which the data lives
    :param sim_filename: sim_filename of the simulation
    :param r_IRC:    radius until which the mean number of particles will be computed. (in angstrom)
    :param rdf_factor: r_IRC*rdf_factor is the outer limit until which the rdf will be computed
    :param output_rdf: whether to compute the rdf and store it as a picture
    :param check_equilibrium: whether to check if IRC is in equilibrium
    :param store_counts: whether to output the raw numpy files too
    :param out_filename : str, general output file name
    :return: N_IRC or N_IRC, N_IRC_early (if check_equilibrium = True)
    """
    print(out_filename)

    delay_range = 0.2  # the fraction of the trajectory to use to check whether the rdf is already "in equilibrium"
    rdf_range = (0., r_IRC * rdf_factor)
    nbins = int(100 * rdf_factor)

    # random init things
    # filename
    filepath = prefix + '/' + sim_filename
    traj_file = f'{filepath}.xtc'
    top_file = f'{filepath}.pdb'

    # create an mdAnalysis Universe
    my_univer = mda.Universe(top_file, traj_file, in_memory=True)

    # select atoms
    a_ats = my_univer.select_atoms(select_A)
    b_ats = my_univer.select_atoms(select_B)

    n_atoms_b = len(b_ats)
    n_atoms_a = len(a_ats)
    print(f"no particles selection a: {n_atoms_a}, \t selection b: {n_atoms_b}")
    volume = np.prod(my_univer.dimensions[:3])

    # if we compute A-A rdfs, we need to exclude the particle itself from the rdf calc
    if (select_A == select_B):
        exclusion_block = (1., 1.)
    else:
        exclusion_block = None

    # compute the full RDF!
    rdf = InterRDF(a_ats, b_ats, verbose=True, range=rdf_range, exclusion_block=exclusion_block, nbins=nbins)
    rdf = rdf.run(start=0)

    # optionally also the early one
    if check_equilibrium:
        eq_idx = int(len(my_univer.trajectory) * delay_range)
        rdf_early = InterRDF(a_ats, b_ats, verbose=True, range=rdf_range, exclusion_block=exclusion_block, nbins=nbins)
        rdf_early = rdf_early.run(start=0, stop=eq_idx)

    if output_rdf:
        # plot the rdf
        plt.figure(figsize=[8, 3])
        plt.title(f'RDF - {select_A} | {select_B}')
        plt.plot(rdf.results.bins, rdf.results.rdf, label='rdf')
        if check_equilibrium:
            plt.plot(rdf_early.results.bins, rdf_early.results.rdf, label='rdf - early')
        plt.grid()
        plt.legend()
        plt.xlim(rdf_range)
        plt.savefig(f'{prefix}/{out_filename}-RDF.png')
        #plt.show()

    if store_counts:
        # save the rdf counts as npy files
        np.save(f'{prefix}/{out_filename}-Counts.npy', rdf.results.count)
        np.save(f'{prefix}/{out_filename}-Vanilla.npy', rdf.results.rdf)
        np.save(f'{prefix}/{out_filename}-Bins.npy', rdf.results.bins)

    # compute the IRC numbers NOW!
    r_IRC_bin_idx = np.argmin(np.abs(rdf.results.bins - r_IRC))

    # compute the number of IRC pairs from the raw counts
    N_IRC = rdf.results.count[:r_IRC_bin_idx].sum()/len(my_univer.trajectory)

    if check_equilibrium:
        # compute the number of IRC pairs from the distance histogram
        N_IRC_early = rdf_early.results.count[:r_IRC_bin_idx].sum() / eq_idx

    # print and return rdf counts
    print(f'{out_filename}-N-IRC: {N_IRC}')
    if check_equilibrium:
        print(f'{out_filename}-N-IRC-early: {N_IRC_early}')

    print()

    if check_equilibrium:
        return N_IRC, N_IRC_early
    else:
        return N_IRC

