import numpy as np
import matplotlib.pyplot as plt
import os

import pandas as pd

from IRCpkg.mixed_system_simulation import mixed_system_simulation
import openmm as omm
import MDAnalysis as mda
from shutil import rmtree
from MDAnalysis.analysis import distances

def plot_potential_string(eps1 : float, eps2: float,
                          r_m : float, r_M : float, name: str = 'test_potential'):
    """
    This will test out the potential string and plot the potential in the file.
    :param potential_string: a string defining a potential based on the openMM syntax
    :param eps1: energy parameter 1 (kbT) (preferable around 1-2 k_bT)
    :param eps2: energy parameter 2 (kbT) (preferable around 1-2 k_bT)
    :param r_m:  distance parameter 1 (angstrom)
    :param r_M:  distance parameter 2 (angstrom) and cutoff for the potential (should be around 1. nm)
    :param name: filename for the plotted potential
    :return:
    """

    if not os.path.exists('./tmp'):
        os.mkdir('./tmp')
    os.chdir('./tmp/')

    T = 298. * omm.unit.kelvin
    kbT = omm.unit.AVOGADRO_CONSTANT_NA*omm.unit.BOLTZMANN_CONSTANT_kB * T

    alpha=1.3
    p_list_df = pd.DataFrame([["A", r_m, 1],["B", r_m, 1]],columns=["name","r_m [A]","count"])
    ff_df = pd.DataFrame([["A", "B", eps2]],columns=["ProteinA","ProteinB","Eps_AB"])

    volume = (2.2*alpha*r_m*omm.unit.angstrom)**3
    prod_T = 5.*(omm.unit.micro*omm.unit.second)
    eq_T = 5.*(omm.unit.micro*omm.unit.second)
    save_file_name,_ = mixed_system_simulation(p_list_df, ff_df, volume, alpha=alpha,
                                                 eps_1=eps1, prod_T=prod_T, eq_T=eq_T,
                                                 saving_freq=10, run_platform="CPU", disable_in_situ_reporting=True)

    traj_file = f'{save_file_name}.xtc'
    top_file = f'{save_file_name}.pdb'

    my_univer = mda.Universe(top_file, traj_file)
    my_np_db = np.genfromtxt(f'{save_file_name}_stdout.txt', delimiter=',', names=True)
    pot_ene = my_np_db['Potential_Energy_kJmole']
    pot_ene /= kbT.in_units_of(omm.unit.kilojoule_per_mole)

    select_A, select_B = my_univer.select_atoms('id 1'), my_univer.select_atoms('id 2')
    universe_dims = my_univer.dimensions
    dist_list = []
    
    for _ in my_univer.trajectory:
        dist_list += [distances.dist(select_A, select_B, box=universe_dims)[-1]]

    plt.scatter(np.asarray(dist_list)/(2*r_m), pot_ene, marker='o', s=0.1)
    plt.scatter((r_M/r_m), 0., marker='o', c='r', label='r_M')
    plt.ylabel('V/kbT')
    plt.xlabel('r/r_m')
    plt.legend()
    plt.grid()
    plt.savefig('../'+ name + '.png')
    plt.xlim([0,4*r_M])
