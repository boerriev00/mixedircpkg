from tqdm import tqdm
import numpy as np
from numba import njit, prange, int32, float64, float32
from IRCpkg.waterMDdistanceCalculator.DistanceCalculator import DistanceCalculator
import MDAnalysis as mda

@njit(int32(float32[:,:], float32[:,:], float32, float32),parallel=True, fastmath=True, nogil=True)
def numba_distances(positions1: np.ndarray, positions2: np.ndarray, box_len: float, r_c_irc: float):
    """
    This calculates the vectors between two sets of atoms using more cpu cores.
    :param positions1: [n_atom1, 3] array of positions of the first set of atoms
    :param positions2: [n_atom2, 3] array of positions of the second set of atoms
    :param box_len: float of the box_size, only supports cubic boxes
    :param r_c_irc: float, irc radius (r_M)
    :return: vectors, with vectors being a [n_atom1, n_atom2, 3] array of vectors, where i,j contains
    the vector from atom i in the first array to atom j in the second array, (only  the upper triangle is calculated)
    """

    # get some basic infos first
    natoms1 = positions1.shape[0]

    positions1 = positions1.astype(float32)
    positions2 = positions2.astype(float32)
    box_len = float32(box_len)
    r_c_irc = float32(r_c_irc)

    n_pairs = int32(0)

    for i in prange(natoms1):
        # get the vectors row wise
        tmp_vecs = positions2[:] - positions1[i]

        # apply pbcs
        tmp_vecs = np.where(tmp_vecs > (box_len / 2), tmp_vecs - box_len, tmp_vecs)
        tmp_vecs = np.where(tmp_vecs < (-box_len / 2), tmp_vecs + box_len, tmp_vecs)

        tmp_distances = np.sqrt((tmp_vecs ** 2).sum(axis=-1))#norm_axis_1(vec_array)#
        mask = tmp_distances != 0.
        n_pairs += (tmp_distances[mask] <= r_c_irc).sum() #- (tmp_distances == 0.).sum()

    return n_pairs

def per_timestep_N_irc(select_A : str, select_B : str, prefix : str, input_filename: str, r_IRC : float, output_file=None):
    """
    Computes the number of particle pairs within r_IRC selected from the universe stored in the folder "prefix"
    with filename "filename" based on the selection rule select_A and select_B.
    Optionally also computes the rdf and stores it as a picture in the current directory.
    Further, optionally also computes rdf and IRC using only the first 20% of the trajectory to see whether the IRC is
    in equilibrium.
    :param select_A: String to select atom group A
    :param select_B: String to select atom group B
    :param prefix:   folder in which the data lives
    :param input_filename: filename of the simulation
    :param r_IRC:    radius until which the mean number of particles will be computed. (in angstrom)
    :param rdf_factor: r_IRC*rdf_factor is the outer limit until which the rdf will be computed
    :param output_rdf: whether to compute the rdf and store it as a picture
    :param check_equilibrium: whether to check if IRC is in equilibrium
    :param trouble: use in case of emergency. (Idk, runs all sorts of troubleshooting infos, like displaying a distance
                    histogram, computing the IRC via the RDF as well, etc.)
    :return: N_IRC or N_IRC, N_IRC_late (if check_equilibrium = True)
    """

    # random init things
    # filename
    filepath = prefix + '/' + input_filename
    traj_file = f'{filepath}.xtc'
    top_file = f'{filepath}.pdb'

    my_univer = mda.Universe(top_file, traj_file, in_memory=True)

    a_ats = my_univer.select_atoms(select_A)
    b_ats = my_univer.select_atoms(select_B)

    box_len = my_univer.dimensions[0]
    r_IRC = float32(r_IRC)
    verlet_radius = 7.5*r_IRC

    dist_calcer = DistanceCalculator(r_IRC, float32(verlet_radius), float32(box_len), a_ats.positions, b_ats.positions)
    print(a_ats.positions.dtype)
    if output_file is None:
        output_file = input_filename + "IRC.npy"

    n_frames = len(my_univer.trajectory)
    output_array = np.zeros((n_frames, 2), dtype=int)

    i = 0
    for frame in tqdm(my_univer.trajectory[:]):
        output_array[i, 0] = frame.time
        distances = dist_calcer.get_nbls_vectors_distances(a_ats.positions, b_ats.positions)
        output_array[i, 1] = (distances <= r_IRC).sum()

        i+=1

    np.save(output_file, output_array)
    return output_array

