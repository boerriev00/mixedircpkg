import argparse
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

"""
This is a small command line tool making plots of the derivatives or Pair Numbers.
folder -> the folder in whjich the simulation_EPS2 file is
key -> name of the pair in the file (e.g. "UL82-UL83"
data -> irc/derivative, whether to plot the pair or derivative vals.
"""

parser = argparse.ArgumentParser()
parser.add_argument("--folder", type=str, default="",
                    help="folder to look through")
parser.add_argument("--key", type=str, default="UL83-UL82",
                    help="key to plot")
parser.add_argument("--data", type=str, default="irc",
                    help="data to plot, either irc or derivative")


args = parser.parse_args()
folder = args.folder
key = args.key
data = args.data

file_list = os.listdir(folder)

if data == "irc":
    IRC_file = [ele for ele in file_list if ele[-10:] == "IRC_AB.txt"][0]
    IRC_df = pd.read_csv(f"./{folder}/{IRC_file}")

    plt.title("Irc evolution on prod run of the pair " + key)
    plt.plot(IRC_df["Step"], IRC_df[key])
    plt.show()

if data == "derivative":
    derivative_file = [ele for ele in file_list if ele[-10:] == "EPS_AB.txt"][0]
    IRC_file = [ele for ele in file_list if ele[-10:] == "IRC_AB.txt"][0]

    derivative_df = pd.read_csv(f"./{folder}/{derivative_file}")
    IRC_df = pd.read_csv(f"./{folder}/{IRC_file}")

    derivative_df = derivative_df.loc[:, key]
    IRC_df = IRC_df.loc[:, key]

    #print(IRC_df)

    len_irc_df = IRC_df.shape[0]
    len_der_df = derivative_df.shape[0]

    if len_irc_df < len_der_df:
        len_traj = len_irc_df
    else:
        len_traj = len_der_df
    N=20
    derivative_array = (derivative_df.mean() * IRC_df.mean() - (IRC_df.values[:len_traj] * derivative_df.values[:len_traj]))
    derivative_array = np.convolve(derivative_array, np.ones(N)/N, mode='valid')
    plt.title("Derivative evolution, rolling average on prod run of the pair " + key)
    plt.plot(derivative_array, "x", alpha=0.1)
    plt.show()

