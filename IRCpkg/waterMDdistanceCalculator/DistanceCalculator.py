import numpy as np
from numba import njit, prange, jit
from IRCpkg.waterMDdistanceCalculator.numba_calculators import numba_make_neighborlist, numba_neighborlist_vectors_distances
from IRCpkg.waterMDdistanceCalculator.numba_calculators import numba_skin_penetrated

class DistanceCalculator:
    def __init__(self, force_radius : float, verlet_radius : float, box_len : float, positionsA : np.ndarray, positionsB : np.ndarray):
        """
        This class will implement a neighborlist based distance calculation
        :param verlet_radius: The neighborlist radius
        :param force_radius: The cutoff radius of the forces
        :param positionsA: The positions of the particles of A type
        :param positionsB: The positions of the particles of B type
        :param box_len: a float specifying pbc box lengths (in angstrom)
        """

        self.box_len = box_len

        # Neighborlist things
        self.verlet_radius = verlet_radius
        self.force_radius = force_radius

        self.neighborlist_idxs = numba_make_neighborlist(positionsA, positionsB,
                                                         self.verlet_radius, self.box_len)

        self.last_nbls_update_posisA = positionsA
        self.last_nbls_update_posisB = positionsB

        self.nbls_update_counter = 0


    def get_nbls_vectors_distances(self, positionsA : np.ndarray, positionsB : np.ndarray):
        """
        :param positionsA: The positions of the particles of A type
        :param positionsB: The positions of the particles of B type
        :return: np.ndarray of shape [n_atoms, n_atoms, 3] with the vector pointing from atom i to j at index i,
        """

        # check for penetrated verlet skin and rebuild the neighborlist if necessary
        if self.nbls_skin_penetrated(positionsA, positionsB):
            self.neighborlist_idxs = numba_make_neighborlist(positionsA, positionsB,
                                                             self.verlet_radius, self.box_len)
            self.last_nbls_update_posisA = positionsA
            self.last_nbls_update_posisB = positionsB
            #print(self.nbls_update_counter)
            self.nbls_update_counter = 0

        distances = numba_neighborlist_vectors_distances(positionsA, positionsB,
                                                                  self.neighborlist_idxs, self.box_len)
        self.nbls_update_counter += 1

        return distances


    def nbls_skin_penetrated(self, positionsA : np.ndarray, positionsB : np.ndarray):
        """
        :param positionsA: The positions of the particles of A type
        :param positionsB: The positions of the particles of B type
        :param verlet_radius: The neighborlist radius
        :param force_radius: The cutoff radius of the forces
        :return: bool, whether the neighborlist skin has been penetrated
        """
        return numba_skin_penetrated(positionsA, positionsB, self.last_nbls_update_posisA, self.last_nbls_update_posisB,
                                     self.verlet_radius - self.force_radius, self.box_len)
