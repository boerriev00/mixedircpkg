import numpy as np
from numba import njit, prange, float32, int32, boolean, threading_layer, set_num_threads
import numba

numba.config.THREADING_LAYER = 'omp'
set_num_threads( numba.config.NUMBA_DEFAULT_NUM_THREADS)
print("Threading layer chosen: %s" % threading_layer())

@njit(float32[:,:](float32[:,:], float32[:,:], float32), parallel=True, fastmath=True, nogil=True)
def numba_vectors_distances(positions1: np.ndarray, positions2: np.ndarray, pbcs: np.ndarray):
    """
    This calculates the vectors between two sets of atoms using more cpu cores.
    :param positions1: [n_atom1, 3] array of positions of the first set of atoms
    :param positions2: [n_atom2, 3] array of positions of the second set of atoms
    :param pbcs: float of the box_sizes
    :return: vectors, with vectors being a [n_atom1, n_atom2, 3] array of vectors, where i,j contains
    the vector from atom i in the first array to atom j in the second array, (only  the upper triangle is calculated)
    """

    # get some basic infos first
    natoms1 = positions1.shape[0]
    natoms2 = positions2.shape[0]
    dimensions = positions1.shape[1]

    # empty arrays for the results
    # vectors = np.zeros((natoms1, natoms2, dimensions))
    distances = np.zeros((natoms1, natoms2), dtype="f4")

    for i in prange(natoms1):
        # get the vectors row wise
        tmp_vecs = positions2[:] - positions1[i]

        # apply pbcs
        tmp_vecs = np.where(tmp_vecs > (pbcs / 2), tmp_vecs - pbcs, tmp_vecs)
        tmp_vecs = np.where(tmp_vecs < (-pbcs / 2), tmp_vecs + pbcs, tmp_vecs)

        distances[i, :] = np.sqrt((tmp_vecs**2).sum(axis=-1))

    return distances

@njit(int32[:,:](float32[:,:], float32[:,:], float32, float32), parallel=False, fastmath=True)
def numba_make_neighborlist(positionsA, positionsB, verlet_radius, pbcs):
    """
    Creates a neighborlist (as an array of indices) and also returns the vectors and distances of the underlying
    calculation
    :param positionsA: a [n_atom_a, 3] np ndarray with atomic positions of type A
    :param positionsB: a [n_atom_b, 3] np ndarray with atomic positions of type B
    :param verlet_radius: the intended verlet radius
    :param pbcs: the box length (only supports cubic boxes!)
    :return: neighbor_list_indices
    with neighbor_list as a [n_neighbor_pairs, 2] np ndarray containing the indices of neighbor atoms

    """
    # get distances first
    distances = numba_vectors_distances(positionsA, positionsB, pbcs)

    # numba doesnt like literals?
    two = 2
    n_atoms_a = positionsA.shape[0]
    n_atoms_b = positionsB.shape[0]

    # make some empty arrays for results
    neighbor_list = np.zeros((n_atoms_a * n_atoms_b, two), dtype='i4')
    idx_list_a = np.arange(n_atoms_a)
    idx_list_b = np.arange(n_atoms_b)
    current_no_idx_pairs = 0

    # for all atoms find partners in upper triangle and fill the atomic indices into the neighbor_list
    for i in idx_list_a:
        partner_idxs = idx_list_b[distances[i,:] <= verlet_radius]

        # make the neighborlist pairs and fill them in
        no_partners = partner_idxs.shape[0]

        neighbor_list[current_no_idx_pairs:current_no_idx_pairs + no_partners, 1] = partner_idxs
        neighbor_list[current_no_idx_pairs:current_no_idx_pairs + no_partners, 0] = i

        # increment to keep track of the total number of pairs
        current_no_idx_pairs += no_partners

    # cut out the nonexistent parts of the list
    neighbor_list = neighbor_list[:current_no_idx_pairs]

    return neighbor_list


@njit(float32[:](float32[:,:], float32[:,:], int32[:,:], float32), parallel=True, fastmath=True, nogil=True)
def numba_neighborlist_vectors_distances(positionsA: np.ndarray, positionsB: np.ndarray,
                                         neighborlist_idxs: np.ndarray, pbcs: float):
    """
    Calculates vectors and distances based on a verlet neighbor list
    :param positions: [n_atom, 3] np.ndarray containing the atomic positions
    :param neighborlist_idxs: [n_neighbor_pais, 2] np.ndarray containing indices of neighbors
    :param pbcs: float of the box_size (only cubic boxes are supported)
    :return:
    """
    # get some general info first
    niter = neighborlist_idxs.shape[0]
    dims = positionsA.shape[1]

    # number of vectors to compute in each (parallel) for loop
    vecs_per_iter = 512

    # number of for loops
    for_iter = niter // vecs_per_iter + 1

    # empty array for results
    #vectors = np.zeros((neighborlist_idxs.shape[0], dims))
    distances = np.zeros((neighborlist_idxs.shape[0]), dtype="f4")

    # somehow splitting the positions into 2 array speeds things up
    a_posis = positionsA[neighborlist_idxs[:, 0]]
    b_posis = positionsB[neighborlist_idxs[:, 1]]

    # iterate through all atoms
    for i in prange(for_iter):

        # current instance starting index
        idx = i * vecs_per_iter
        # number of vecs for this loop instance
        offset = vecs_per_iter

        # careful with array borders
        if i == for_iter - 1:
            offset = niter - idx

        # calculate all vectors and distances in one batch, np vectorized ops are fast
        tmp_vecs = a_posis[idx:idx + offset] - b_posis[idx:idx + offset]

        # apply pbcs
        tmp_vecs = np.where(tmp_vecs > (pbcs / 2), tmp_vecs - pbcs, tmp_vecs)
        tmp_vecs = np.where(tmp_vecs < (-pbcs / 2), tmp_vecs  + pbcs, tmp_vecs)

        #vectors[idx:idx+offset] = tmp_vecs
        distances[idx:idx+offset] = np.sqrt((tmp_vecs**2).sum(axis=-1))

    return distances

@njit(boolean(float32[:,:], float32[:,:],float32[:,:], float32[:,:], float32, float32),parallel=False, fastmath=True)
def numba_skin_penetrated(positionsA : np.ndarray, positionsB : np.ndarray,
                          old_positionsA : np.ndarray, old_positionsB : np.ndarray,
                          skin_radius : float, box_len : float):

    # calc the distance travelled after the last update:
    vec_displacements_A = old_positionsA - positionsA
    vec_displacements_B = old_positionsB - positionsB

    vec_displacements_A = np.where(vec_displacements_A > (box_len / 2), vec_displacements_A - box_len,
                                   vec_displacements_A)
    vec_displacements_A = np.where(vec_displacements_A < (-box_len / 2), vec_displacements_A + box_len,
                                   vec_displacements_A)

    vec_displacements_B = np.where(vec_displacements_B > (box_len / 2), vec_displacements_B - box_len,
                                   vec_displacements_B)
    vec_displacements_B = np.where(vec_displacements_B < (-box_len / 2), vec_displacements_B + box_len,
                                   vec_displacements_B)

    # get displacements from that
    displacement_lengths_A = np.sqrt((vec_displacements_A ** 2).sum(axis=-1))
    displacement_lengths_B = np.sqrt((vec_displacements_B ** 2).sum(axis=-1))

    return (displacement_lengths_A > skin_radius).any() or (displacement_lengths_B > skin_radius).any()
