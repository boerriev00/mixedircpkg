#!/usr/bin/bash
#SBATCH -J LogArmijo
#SBATCH -o LogArmijo.out 
#SBATCH --partition=gpu
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=3
#SBATCH --mem=22000M 
#SBATCH --gres=gpu:1
#SBATCH --time=7-00:00:00 
#SBATCH --exclude=gpu[048-100]
#SBATCH --mail-type=end 
#SBATCH --mail-user=boerriev00@zedat.fu-berlin.de  
#SBATCH --export=ALL

python -u testRun.py > LogArmijo.txt 
