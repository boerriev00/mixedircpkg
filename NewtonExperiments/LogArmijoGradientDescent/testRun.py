import pandas as pd
from IRCpkg.NewtonMethod.algorithm import gradient_descent_iteration
import warnings
warnings.filterwarnings("ignore")
import pathlib

linesearch_algo = "armijo"
loss_func = "LOG"

path = str(pathlib.Path(__file__).parent.parent.resolve())

FF_df = pd.read_csv(path+"/testReferenceFF_df.csv", index_col=0)
para_file = path+"/defaultParaFile.csv"
protein_file = path+"/exampleProteinDF.csv"

gradient_descent_iteration(FF_df, FF_df["N_pairs"], para_file, protein_file, "jacobRun", seed=42, linesearch_algo=linesearch_algo, loss_func=loss_func)