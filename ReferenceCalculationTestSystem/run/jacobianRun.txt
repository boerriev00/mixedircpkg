equilibration time [mu s]           1.0
production time [mu s]           1000.0
sampling timestep [ns]              4.0
integration timestep [ps]          25.0
damping constant [1/ps]           847.1
simulation alpha                    1.3
volume [nm**3]               12000000.0
Name: 0, dtype: float64

    name    r_m [A]  count
0  UL100  28.201078   2212
1   UL25  37.087557   2268
2   UL82  31.575309   8120
3   UL83  30.688034   8376

  ProteinA ProteinB    Eps_AB
0     UL83    UL100 -2.160730
1     UL83     UL25 -3.993441
2     UL83     UL82 -2.253403
3     UL83     UL83 -0.809791
4     UL82    UL100 -1.298799
5     UL82     UL25  0.431794
6     UL82     UL82 -3.106198
7     UL25    UL100 -2.308524
8     UL25     UL25  0.557606
9    UL100    UL100 -0.623422

<Element PX>
<Residue 0 (UL100) of chain 0> with id:1

<Element PX>
<Residue 1 (UL25) of chain 0> with id:2

<Element PX>
<Residue 2 (UL82) of chain 0> with id:3

<Element PX>
<Residue 3 (UL83) of chain 0> with id:4

Eps2-UL100-UL100 : -1544.6598208105738 J/mol
Eps2-UL100-UL25 : -5719.852891496539 J/mol
Eps2-UL100-UL82 : -3218.047282387042 J/mol
Eps2-UL100-UL83 : -5353.66198679662 J/mol
Eps2-UL25-UL25 : 1381.5868369793884 J/mol
Eps2-UL25-UL82 : 1069.8592390481022 J/mol
Eps2-UL25-UL83 : -9894.587813726794 J/mol
Eps2-UL82-UL82 : -7696.258366824828 J/mol
Eps2-UL82-UL83 : -5583.2798191142565 J/mol
Eps2-UL83-UL83 : -2006.4273461069356 J/mol
No of steps 40000000
No of saved steps 250000.0

Setup Done!
Equilibration Done!
sim_epsAB_day17_mon4 ; {'UL100': 1, 'UL25': 2, 'UL82': 3, 'UL83': 4} ;
elapsed time [s]: 18789.811193227768
{'UL100': 1, 'UL25': 2, 'UL82': 3, 'UL83': 4}
DONE!
