#!/usr/bin/bash 
#SBATCH -J RefSimTest  
#SBATCH -o RefSimTest.out 
#SBATCH --partition=gpu
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=3
#SBATCH --mem=6000M 
#SBATCH --gres=gpu:1
#SBATCH --time=7-00:00:00 
#SBATCH --mail-type=end 
#SBATCH --mail-user=boerriev00@zedat.fu-berlin.de  
#SBATCH --export=ALL

python -u runSimu.py > runSimu.txt 
